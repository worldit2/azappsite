﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AzappSite.Models
{
    public class MailChimpModel
    {

        [Required]
        public string emails { get; set; }

        [Required]       
        public string Language { get; set; }

        [Required]     
        public string Email { get; set; }
       

    }

    public class MailChimpResult
    {
        public string type { get; set; }
        public string title { get; set; }
        public int status { get; set; }
        public string detail { get; set; }
        public string instance { get; set; }
    }

    public class MailChimpAddMemberModel
    {
        public string email_address { get; set; }
        public string status { get; set; }
        public string language { get; set; }
    }
    }
