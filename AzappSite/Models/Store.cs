﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AzappSite.Models
{
    public class Store
    {
        [Required]
        public string CompanyUserNameField { get; set; }


        [Required]
        public string PhoneNumberField { get; set; }


        [Required, EmailAddress]
        public string EmailField { get; set; }


        //[Required]
        //public string AppNameField { get; set; }


        [Required]
        public string DescriptionField { get; set; }

        //[Required]
        //public string AppUrlField { get; set; }

        //public string EmailAppField { get; set; }
      
        //public string AppPasswordField { get; set; }


        //[Required]
        //public string AppKeywordField { get; set; }

        [Required]
        public List<String> FuncionalitiesLabelField { get; set; }

    }
}