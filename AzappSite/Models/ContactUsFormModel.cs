﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AzappSite.Models
{
    public class ContactUsFormModel
    {

        [Required]
        public string UserNameField { get; set; }


        [Required]
        public string TypeField { get; set; }


        [Required, EmailAddress]
        public string EmailField2 { get; set; }


        [Required]
        public string DescriptionField2 { get; set; }


    }
}