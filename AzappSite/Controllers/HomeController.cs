﻿using AzappSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Http.Results;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace AzappSite.Controllers
{
    public class HomeController : SurfaceController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ContactSubmit([Bind(Include = "UserNameField, TypeField, EmailField2, DescriptionField2")] ContactUsFormModel mailModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var del = new DelegateSendMailForm(SendFormEmail);
                    var invoke = del.BeginInvoke(mailModel, null, null);
                }
            }
            catch { }
            return Json(mailModel);
        }

        public delegate void DelegateSendMailForm(ContactUsFormModel mailModel);

        public void SendFormEmail(ContactUsFormModel mailModel)
        {
            try
            {
                String mailLuis = WebConfigurationManager.AppSettings["MailLuis"];

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                NetworkCredential basicCredential = new NetworkCredential("support_event@worldit.pt", "supportevent123");
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential;
                client.Host = "mail.worldit.pt";
                mail.To.Add(new MailAddress(mailLuis));
                client.Timeout = 200000;

                mail.From = new MailAddress(mailModel.EmailField2);
                mail.Subject = "Contact Us: " + mailModel.TypeField;
                mail.IsBodyHtml = true;
                var body = String.Empty;
                body += String.Format(@"<b>Nome:</b> {0}<br/>", mailModel.UserNameField);
                body += String.Format(@"<b>Descrição:</b> {0}<br/>", mailModel.DescriptionField2);
                mail.Body = body;
                client.Send(mail);
                ViewBag.SUCCESS = "Message Sent";
            }
            catch (Exception e)
            {
                string result = e.Message + ";";
                while (e.InnerException != null)
                {
                    result += e.InnerException.Message + ";";
                    e = e.InnerException;
                }
                ViewBag.SUCCESS = "Error sending message";
                RedirectToAction("Index", "Error", new { message = result });
            }
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult StoreSubmit([Bind(Include = "CompanyUserNameField, PhoneNumberField, EmailField, DescriptionField, FuncionalitiesLabelField")] Store mailModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var del = new DelegateSendMailFormStore(SendStoreFormEmail);
                    var invoke = del.BeginInvoke(mailModel, null, null);
                }
            }
            catch { }
            return Json(mailModel);
        }

        public delegate void DelegateSendMailFormStore(Store mailModel);

        public void SendStoreFormEmail(Store mailModel)
        {
            try
            {
                String mailLuis = WebConfigurationManager.AppSettings["MailLuis"];
                var funcionalities = "<br/>";

                foreach (var item in mailModel.FuncionalitiesLabelField)
                {
                    funcionalities += item;
                    funcionalities += "<br/>";
                }
                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                NetworkCredential basicCredential = new NetworkCredential("support_event@worldit.pt", "supportevent123");
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential;
                client.Host = "mail.worldit.pt";
                mail.To.Add(new MailAddress(mailLuis));
                client.Timeout = 200000;

                mail.From = new MailAddress(mailModel.EmailField);
                mail.Subject = "Store: Solicitação de Aplicação";
                mail.IsBodyHtml = true;
                var body = String.Empty;
                body += String.Format(@"<b>Nome do requerente/empresa</b>: {0}<br/><br/>", mailModel.CompanyUserNameField);
                body += String.Format(@"<b>Telefone</b>: {0}<br/><br/>", mailModel.PhoneNumberField);
                //body += String.Format(@"<b>Nome da aplicação</b>: {0}<br/><br/>", mailModel.AppNameField);
                //body += String.Format(@"<b>URL associado à aplicação</b>: {0}<br/><br/>", mailModel.AppUrlField);
                //body += String.Format(@"<b>E-mail associado à aplicação</b>: {0}<br/><br/>", mailModel.EmailAppField);
                //body += String.Format(@"<b>Palavra passe</b>: {0}<br/><br/>", mailModel.AppPasswordField);
                //body += String.Format(@"<b>Palavras chave da Aplicação</b>: {0}<br/><br/>", mailModel.AppKeywordField);
                body += String.Format(@"<b>Funcionalidades</b>: {0}<br/><br/>", funcionalities);
                body += String.Format(@"<b>Descrição da aplicação</b>: {0}<br/>", mailModel.DescriptionField);
                mail.Body = body;
                client.Send(mail);
                ViewBag.SUCCESS = "Message Sent";


                MailMessage mail2 = new MailMessage();
                SmtpClient userReply = new SmtpClient();
                userReply.Port = 25;
                userReply.DeliveryMethod = SmtpDeliveryMethod.Network;
                NetworkCredential basicCredential2 = new NetworkCredential("support_event@worldit.pt", "supportevent123");
                userReply.UseDefaultCredentials = false;
                userReply.Credentials = basicCredential;
                userReply.Host = "mail.worldit.pt";
                mail2.To.Add(new MailAddress(mailModel.EmailField));
                userReply.Timeout = 200000;
                mail2.From = new MailAddress("support_event@worldit.pt");

                string htmlMessage = @"<html>
                         <body>
                          O seu pedido foi recebido com sucesso. Entraremos em contacto consigo em breve.<br/><br/>
                          Com os melhores cumprimentos.<br/><br/><br/>
                         <img src='cid:AzappLogoMail' />
                         </body>
                         </html>";

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(
                                               htmlMessage,
                                               Encoding.UTF8,
                                               MediaTypeNames.Text.Html);

                string mediaType = MediaTypeNames.Image.Jpeg;
                LinkedResource img = new LinkedResource(Server.MapPath(@"\\Media\\img\\AzappLogoMail.jpg"), mediaType);

                img.ContentId = "AzappLogoMail";
                img.ContentType.MediaType = mediaType;
                img.TransferEncoding = TransferEncoding.Base64;
                img.ContentType.Name = img.ContentId;
                img.ContentLink = new Uri("cid:" + img.ContentId);
                htmlView.LinkedResources.Add(img);
                mail2.AlternateViews.Add(htmlView);

                mail2.Subject = "[No Reply] Azapp: Solicitação de Aplicação";
                mail2.IsBodyHtml = true;

                userReply.Send(mail2);
            }
            catch (Exception e)
            {
                string result = e.Message + ";";
                while (e.InnerException != null)
                {
                    result += e.InnerException.Message + ";";
                    e = e.InnerException;
                }
                ViewBag.SUCCESS = "Error sending message";
                RedirectToAction("Index", "Error", new { message = result });
            }
        }
    }
}