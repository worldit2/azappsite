﻿using AzappSite.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace AzappSite.Models
{
    public class FormMailChimpController : SurfaceController
    {
        // GET: Contactos
        //public ActionResult Index() {
        //    return PartialView("FormMailChimp");
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<String> HandleFormPost(string email, string language)
        {
            try
            {
                var apiKey = ConfigurationManager.AppSettings["MailChimpAPIKey"].ToString();
                var listID = ConfigurationManager.AppSettings["MailChimpListID"].ToString();
                string username = ConfigurationManager.AppSettings["MailChimpUsername"].ToString();
                var apiKeyHashed = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(username + ":" + apiKey));
                //API STUFF
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", apiKeyHashed);
                    //var url = "https://us18.api.mailchimp.com/3.0/lists/" + listID + "/members";
                    var url = "https://us20.api.mailchimp.com/3.0/lists/" + listID + "/members";

                    var member = new MailChimpAddMemberModel()
                    {
                        email_address = email,
                        status = "subscribed",
                        language = language,
                    };

                    var httpResponse = await httpClient.PostAsJsonAsync(url, member);

                    var homeVariable = Umbraco.ContentAtRoot().FirstOrDefault();
                    var mailChimp = homeVariable.DescendantOfType("socialNetworks");

                    if ((int)httpResponse.StatusCode != 200)
                    {
                        var contents = httpResponse.Content.ReadAsStringAsync().Result;
                        MailChimpResult mailChimpResult = JsonConvert.DeserializeObject<MailChimpResult>(contents);
                        var error = string.Empty;
                        if (mailChimpResult.status == 400)
                        {
                            error = mailChimp.Value("errorMemberExists").ToString();
                        }
                        else
                        {
                            error = mailChimp.Value("errorAlertText").ToString();
                        }
                        throw new Exception(error);
                    }
                    return "{\"error\":\"false\",\"message\":\"" + mailChimp.Value("successAlertText") + "\"}";
                }
            }
            catch (Exception ex)
            {
                return "{\"error\":\"true\",\"message\":\"" + ex.Message + "\"}";
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public string UmbracoMembers(string email, string language, string emails)
        {
            try
            {

                var memberService = Services.MemberService;
                var memberGroupService = Services.MemberGroupService;
                var homeVariable = Umbraco.ContentAtRoot().FirstOrDefault();
                var socialNetworks = homeVariable.DescendantOfType("socialNetworks");
                var error = string.Empty;

                if (memberService.GetByEmail(email) != null)
                {
                    error = socialNetworks.Value("errorMemberExists").ToString();
                    throw new Exception(error);
                }
                else
                {
                    var newMember = memberService.CreateMemberWithIdentity(email, email, email, "Member");
                    newMember.IsApproved = true;
                    memberService.SavePassword(newMember, email);
                    if (language.Equals("Portuguese") || language.Equals("Português"))
                    {
                        memberService.AssignRole(newMember.Id, "NEWSLETTER-PT");
                    }
                    else if (language.Equals("English") || language.Equals("Inglês"))
                    {
                        memberService.AssignRole(newMember.Id, "NEWSLETTER-EN");
                    }
                    memberService.Save(newMember);
                    if(memberService.GetByEmail(email) == null)
                    {
                        error = socialNetworks.Value("errorAlertText").ToString();
                        throw new Exception(error);
                    }
                    else
                    {
                        var del = new DelegateSendMailForm(SendFormEmail);
                        var invoke = del.BeginInvoke(email, emails, null, null);
                    }
                    return "{\"error\":\"false\",\"message\":\"" + socialNetworks.Value("successAlertText") + "\"}";
                }
            }
            catch (Exception ex)
            {
                return "{\"error\":\"true\",\"message\":\"" + ex.Message + "\"}";
            }
        }


        public delegate void DelegateSendMailForm(string email, string emails);


        public void SendFormEmail(string email, string emailsUmbraco)
        {
            try
            {
                             
                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                NetworkCredential basicCredential = new NetworkCredential("support_event@worldit.pt", "supportevent123");
                client.UseDefaultCredentials = false;
                client.Credentials = basicCredential;
                client.Host = "mail.worldit.pt";

                foreach (var item in emailsUmbraco.Split(','))
                {
                    mail.To.Add(item);
                }
                    client.Timeout = 200000;
                    mail.From = new MailAddress(email);
                    mail.Subject = "Newsletter member registration";
                    mail.IsBodyHtml = true;
                    var body = String.Empty;
                    body += String.Format(@"I subscribed your newsletter.");               
                    mail.Body = body;
                    client.SendMailAsync(mail);       
            }
            catch (Exception e)
            {
                string result = e.Message + ";";
                while (e.InnerException != null)
                {
                    result += e.InnerException.Message + ";";
                    e = e.InnerException;
                }
                ViewBag.SUCCESS = "Error sending message";
                RedirectToAction("Index", "Error", new { message = result });
            }
        }
    }
}